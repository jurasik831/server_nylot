package main

import (
	// "fmt"
	"net"
)

type User struct {
	addr              net.UDPAddr
	In_game           bool
	PacketNumTo       uint32
	MaxPacketNumFrom  uint32
	MaskPacketFrom    uint16
	Room_id           int
	is_fost           byte
	last_request_time int64
}

type Users struct {
	Us []*User
}

func (us Users) Is_exists_user(address net.UDPAddr) bool {
	var is_exist bool = false
	if us.Get_user_id(address) != -1 {
		is_exist = true
	}
	return is_exist
}

func (us Users) Get_user_id(address net.UDPAddr) int {
	var user_id int = -1
	for id, user := range us.Us {
		if user == nil {
			continue
		}
		if address.String() == user.addr.String() {
			user_id = id
		}
	}
	return user_id
}

func (u *User) Add_net_info(packet Packet) {

	if packet.Tech_part.LocalPacketNum > u.MaxPacketNumFrom {
		u.MaskPacketFrom = (1 | (u.MaskPacketFrom << (packet.Tech_part.LocalPacketNum - u.MaxPacketNumFrom)))
		u.MaxPacketNumFrom = packet.Tech_part.LocalPacketNum
	} else {
		u.MaskPacketFrom = u.MaskPacketFrom | (1 << (packet.Tech_part.LocalPacketNum - u.MaxPacketNumFrom))
	}

}

func (u *User) Next_packet_num() uint32 {
	u.PacketNumTo += 1
	return u.PacketNumTo
}
