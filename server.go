package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

const UDP_STRING string = "udp"
const ADR_UDP_PTR string = "%s:%d"
const MAX_BYTES_IN_PACKET int = 1500
const FRAME_LEN int64 = 16 * int64(time.Millisecond)
const DISC_TIMEOUT int64 = 10 * int64(time.Second)

const IS_TEST bool = false

type Server struct {
	*net.UDPConn
	buf   []byte
	Users *Users
	Rooms *Rooms

	last_msg_len int
	frames       uint32
	info_log     log.Logger
	wrn_log      log.Logger
	err_log      log.Logger
	test_log     log.Logger
	is_read      bool
}

func (s *Server) initLogger(log_f_p string) {
	file, err := os.OpenFile(log_f_p, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	s.info_log = *log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	s.wrn_log = *log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	s.err_log = *log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	s.test_log = *log.New(file, "TEST: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func NewConn(port int, ip string, log_f_p string) *Server {

	ServerAddr, err := net.ResolveUDPAddr(UDP_STRING, fmt.Sprintf(ADR_UDP_PTR, ip, port))
	if err != nil {
		panic(err)
	}
	ServerConn, err := net.ListenUDP(UDP_STRING, ServerAddr)
	if err != nil {
		panic(err)
	}

	s := Server{UDPConn: ServerConn, buf: make([]byte, MAX_BYTES_IN_PACKET), Users: &Users{}, Rooms: &Rooms{}, is_read: false}
	s.initLogger(log_f_p)

	return &s
}

func (s Server) Start() {

	go s.frame_counter()

	for {

		// go s.every_frame(ch_server_frame)
		go s.reading()
		time.Sleep(1 * time.Millisecond)

	}
}

func (s *Server) reading() {
	if !s.is_read {
		s.is_read = true
		pack_len, addr, err := s.ReadFromUDP(s.buf)

		// for _, uu := range s.Users.Us {
		// 	fmt.Println(uu.addr.Port)
		// }

		if err != nil {
			panic(err)
		}
		s.client_init(*addr)
		// Maybe truble with rewrite in multi-thread
		s.last_msg_len = pack_len
		s.m_handler(s.Users.Get_user_id(*addr))
		s.is_read = false
	}

}

func (s Server) client_init(addr net.UDPAddr) {
	if !s.Users.Is_exists_user(addr) {
		fmt.Println("Init")
		s.add_user_to(addr)
	}
}

func (s *Server) join_game(user_id int) (byte, int) {
	s.Users.Us[user_id].In_game = true
	return s.Rooms.Add_to_room(user_id)
}

func (s *Server) every_frame(c uint32) {
	for id, user := range s.Users.Us {
		if user == nil || user.Room_id == -1 {
			continue
		}

		if time.Now().UnixNano()-user.last_request_time >= DISC_TIMEOUT {
			if user.is_fost == 1 {
				users_in_room := s.Rooms.delete_room(user.Room_id)
				for _, user_in_room := range users_in_room {
					if user_in_room != -1 {
						s.Users.Us[user_in_room].Room_id = -1
					}
				}
			} else {
				s.Rooms.delete_user_from_room(user.Room_id, id)
			}
			s.Users.Us[id] = nil
		}

	}
}

func (s *Server) frame_counter() {
	var last_check, iteration_start, dif_between_iteration int64
	var rem_from_prev_iteration int64 = 0
	var testing_t, testing_n time.Time

	if IS_TEST {
		testing_t = time.Now()
		last_check = testing_t.UnixNano()

	} else {
		last_check = time.Now().UnixNano()
	}

	for {
		if IS_TEST {
			testing_n = time.Now()
			iteration_start = testing_n.UnixNano()
		} else {
			iteration_start = time.Now().UnixNano()
		}

		dif_between_iteration = iteration_start - last_check
		dif_between_iteration += rem_from_prev_iteration

		dif_between_iteration = dif_between_iteration - FRAME_LEN
		for dif_between_iteration >= 0 {
			s.frames++
			ch_server_frame = s.frames
			s.every_frame(ch_server_frame)
			dif_between_iteration = dif_between_iteration - FRAME_LEN
		}
		if IS_TEST {
			s.test_log.Printf("%d - %d\r\n", s.frames, testing_n.Sub(testing_t).Milliseconds()/16)
		}

		last_check = iteration_start

		rem_from_prev_iteration = dif_between_iteration + FRAME_LEN
		time.Sleep(1 * time.Millisecond)
	}
}

func (s Server) broadcast_all_last_msg() {
	for _, user := range s.Users.Us {
		if user == nil {
			continue
		}
		s.WriteToUDP(s.buf, &user.addr)
	}
}

func (s *Server) Send_to(user *User, packet *Packet) {
	packet.Tech_part.Destination = 0
	// packet.Tech_part.FixedFrame = 0
	packet.Tech_part.LocalPacketNum = user.Next_packet_num()
	packet.Tech_part.RemoteMask = user.MaskPacketFrom
	packet.Tech_part.RemoteMaxPacketNum = user.MaxPacketNumFrom
	s.WriteToUDP(packet.Get_bytes(), &user.addr)
}

func (s *Server) add_user_to(addr net.UDPAddr) int {

	nu := new(User)
	nu.addr = addr

	s.Users.Us = append(s.Users.Us, nu)

	return len(s.Users.Us) - 1
}

func (s *Server) Get_ip() {
	addrs, _ := net.InterfaceAddrs()
	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				fmt.Println(ipnet.IP.String())
			}
		}
	}
}

func (s Server) m_handler(user_id int) {
	var packet Packet
	var host byte = 0
	packet.Parse(s.buf[0:s.last_msg_len])
	// fmt.Println(packet.Command())
	user := s.Users.Us[user_id]
	user.last_request_time = time.Now().UnixNano()
	switch packet.Command() {
	case COMMAND_INITIALIZATION:
		in_game_id, room_id := s.join_game(user_id)
		user.Room_id = room_id
		user.Add_net_info(packet)
		send_packet := new(Packet)
		if in_game_id == NET_ID_PLAYER_EIGHT {
			host = 1
		}
		user.is_fost = host
		send_packet.Add_to_payload(COMMAND_CONNECT, ch_server_frame-s.Rooms.Rooms[room_id].Room_create_frame, in_game_id, host)
		send_packet.Tech_part.FixedFrame = 0
		s.Send_to(user, send_packet)
		break
	case COMMAND_MESSAGE:
		if user.Room_id != -1 {
			for _, another_user_id := range s.Rooms.Rooms[user.Room_id].Users {
				if another_user_id != -1 && another_user_id != user_id {
					s.Send_to(s.Users.Us[another_user_id], &Packet{Payload: packet.Payload})
				}
			}
		}

		// s.broadcast_all_last_msg()
	case COMMAND_NET_SYNC:
		if user.Room_id != -1 {
			for _, another_user_id := range s.Rooms.Rooms[user.Room_id].Users {
				if another_user_id != -1 && another_user_id != user_id {
					s.Send_to(s.Users.Us[another_user_id], &Packet{Payload: packet.Payload, Tech_part: Packet_tech_part{FixedFrame: packet.Tech_part.FixedFrame}})
				}
			}
		}

		// s.broadcast_all_last_msg()
	}
	// fmt.Println("Received ", string(packet.Payload))

}

func (s Server) Close_server() {
	s.Close()
}
