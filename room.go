package main

import (
	"math"
)

const NET_ID_PLAYER_ONE byte = 1
const NET_ID_PLAYER_TWO byte = 2
const NET_ID_PLAYER_THREE byte = 4
const NET_ID_PLAYER_FOUR byte = 8
const NET_ID_PLAYER_FIVE byte = 16
const NET_ID_PLAYER_SIX byte = 32
const NET_ID_PLAYER_SEVEN byte = 64
const NET_ID_PLAYER_EIGHT byte = 128

type Room struct {
	Users             [8]int
	Room_create_frame uint32
}

type Rooms struct {
	Rooms []*Room
}

func (r *Rooms) Add_to_room(user_id int) (byte, int) {
	room_id, slot_id := r.free_slot_room()
	r.Rooms[room_id].Users[slot_id] = user_id

	return byte(math.Pow(2, float64(slot_id))), room_id
}

func (r *Rooms) free_slot_room() (int, int) {
	var id_room_with_free_slot int = -1
	var slot_id int = -1
	for id, room := range r.Rooms {
		if room == nil {
			continue
		}
		if slot_id = room.has_free_slot(); slot_id != -1 {
			id_room_with_free_slot = id
		}
	}

	if id_room_with_free_slot == -1 {
		id_room_with_free_slot = r.add_room()
		slot_id = r.Rooms[id_room_with_free_slot].has_free_slot()
	}
	return id_room_with_free_slot, slot_id
}

func (r *Rooms) delete_room(id int) [8]int {
	users_in_room := r.Rooms[id].Users
	r.Rooms[id] = nil
	return users_in_room
}

func (r *Rooms) delete_user_from_room(room_id int, user_id int) {
	for id, val := range r.Rooms[room_id].Users {
		if val == user_id {
			r.Rooms[room_id].Users[id] = -1
		}
	}
}

func (r *Rooms) add_room() int {
	r.Rooms = append(r.Rooms, &Room{Users: [8]int{-1, -1, -1, -1, -1, -1, -1, -1}, Room_create_frame: ch_server_frame})
	return len(r.Rooms) - 1
}

func (r *Room) has_free_slot() int {
	var free_slot_id int = -1
	for id, user_id := range r.Users {
		if user_id == -1 {
			free_slot_id = id
		}
	}
	return free_slot_id
}
