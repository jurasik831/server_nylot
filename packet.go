package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

const TECHNICAL_PART_LENGHT uint16 = 15

const COMMAND_INITIALIZATION byte = 0

const COMMAND_CONNECT byte = 0
const COMMAND_NET_SYNC byte = 16
const COMMAND_MESSAGE byte = 128

type Packet struct {
	Tech_part Packet_tech_part
	Payload   []byte
}

type Packet_tech_part struct {
	LocalPacketNum     uint32
	RemoteMaxPacketNum uint32
	RemoteMask         uint16
	FixedFrame         uint32
	Destination        byte
}

func (p *Packet) Parse(udp_packet []byte) {
	err := binary.Read(bytes.NewBuffer(udp_packet[0:TECHNICAL_PART_LENGHT]), binary.BigEndian, &p.Tech_part)
	if err != nil {
		panic(err)
	}
	p.Payload = udp_packet[TECHNICAL_PART_LENGHT:]
	// fmt.Println(p)
}

func (p *Packet) Command() byte {
	var command byte

	if p.is_init() {
		command = COMMAND_INITIALIZATION
	} else {
		if len(p.Payload) != 0 {
			command = p.Payload[0]
		}
	}

	return command
}

func (p *Packet) is_init() bool {
	var is_init bool = false

	if p.Tech_part.Destination == 0 && p.Tech_part.FixedFrame == 0 && p.Tech_part.RemoteMask == 0 && p.Tech_part.RemoteMaxPacketNum == 0 && len(p.Payload) == 0 {
		is_init = true
	}

	return is_init
}

func (p *Packet) Add_to_payload(a ...interface{}) []byte {
	var buf []byte
	var to_send *bytes.Buffer

	to_send = bytes.NewBuffer(buf)
	for _, part := range a {

		binary.Write(to_send, binary.BigEndian, part)
	}
	p.Payload = to_send.Bytes()
	fmt.Println(p.Payload, " - payload")
	return to_send.Bytes()
}

func (p *Packet) Get_bytes() []byte {
	var tech_bytes []byte
	var to_send_bytes []byte
	to_send_tech := bytes.NewBuffer(tech_bytes)
	binary.Write(to_send_tech, binary.BigEndian, p.Tech_part)
	to_send_bytes = append(to_send_tech.Bytes(), p.Payload...)
	return to_send_bytes
}
